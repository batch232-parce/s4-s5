package com.zuitt.example;

public class Child extends Parent{
    public Child(){
        super();
    }

    public void speak(){
        System.out.println("I am a child");
    }
}
