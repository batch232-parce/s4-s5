package com.zuitt.example;

public class Parent {
    // Mini activity
    // Static Polymorphism within greet methods
    public void greeting(){
       System.out.println("Hello friends!");
    }
    public void greeting(String name, String timeOfTheDay){
       System.out.println("Good " + timeOfTheDay + name);
    }

    public void speak(){
        System.out.println("I am the parent");
    }
}
