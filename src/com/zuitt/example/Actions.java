package com.zuitt.example;

public interface Actions {
    // interface - blueprints for your classes, any class that implements our interface must have the methods in the interface
    public void sleep();
    public void run();


}
