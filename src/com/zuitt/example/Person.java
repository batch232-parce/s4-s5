package com.zuitt.example;

public class Person implements Actions, Greetings {
    public void sleep(){
        System.out.println("Zzzzzzzzzz...");
    }

    public void run(){
        System.out.println("Lezzzgoooo");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays!!");
    }
}
